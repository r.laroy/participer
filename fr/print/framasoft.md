# Outils de communication <i lang ="en">print</i> pour <b class="violet">Frama</b><b class="orange">soft</b>

**Nous avons différents formats possibles :**

  * Les flyers (présentation courte) ;
  * Les dépliants (présentation plus complète) ;
  * Les stickers (autocollants) ;
  * Les kakemono (ou Roll-up, pour des stands, etc.).


## Flyer

### Présentation de <b class="violet">Frama</b><b class="orange">soft</b>

**Format**

  * DL (21 x 10,5 cm) paysage, recto-verso, couleurs
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: DejaVu Sans

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/flyer_recto.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/flyer_recto.png" /></a>
        <p class="text-center">Recto</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="images/flyer_verso.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/flyer_verso.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>


## Dépliant

### Présentation de <b class="violet">Frama</b><b class="orange">soft</b>

**Format**

  * 10 x 21 cm fermé, A4 paysage (21 x 29,7 cm) ouvert, 2 plis roulés, 3 volets, recto-verso, couleurs
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: Lato, DejaVu Sans


**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/frama_plaquette_recto.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/frama_plaquette_recto.png" /></a>
        <p class="text-center">Recto</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="images/frama_plaquette_verso.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/frama_plaquette_verso.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>

## Stickers

### Rond <b class="violet">Frama</b><b class="orange">soft</b>

**Format**

  * Rond 6 cm, recto couleurs, verso N&B
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Police d'écriture utilisé&nbsp;: DejaVu Sans.

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="images/Framasoft_Sticker_rond.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/Framasoft_Sticker_rond.png" /></a>
        <p class="text-center">Recto</i></p>
    </div>
    <div class="col-sm-6">
        <a href="images/Framasoft_Sticker_back02.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/Framasoft_Sticker_back02.png" /></a>
        <p class="text-center"><i lang="en">Verso (facultatif)</p>
    </div>
</div>

## Kakemonos

### <b class="violet">Frama</b><b class="orange">soft</b>

**Format**

  * 85 x 200 cm, portrait, recto, couleurs
  * Polices d'écritures utilisées&nbsp;: DejaVu Sans

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/kakemono_framasoft.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/kakemono_framasoft.png" /></a>
        <p class="text-center">Kakemono</p>
    </div>
</div>

### Les mondes de <b class="violet">Frama</b><b class="orange">soft</b>

**Format**

  * 85 x 200 cm, portrait, recto, couleurs
  * Polices d'écritures utilisées&nbsp;: BurbinCasual NC, Lato
  * CC-By-SA Geoffrey Dorne

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/kakemono-infographie_framasoft_2017.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/kakemono-infographie-dorne-jpg.png" /></a>
        <p class="text-center">Kakemono</p>
    </div>
</div>
