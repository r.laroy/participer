# Réalisation du lanceur et paquetage de l’application

**Rappel :** nous sommes dans le cas le plus simple où l’application est
nativement portable et discrète. Dans d’autres cas, la procédure qui
suit sera plus complexe.

## 1. Définition des principales informations

Le pack de portabilisation que vous avez normalement téléchargé en début
de ce tutoriel intègre un logiciel qui va vous aider à réaliser le
lanceur et le paquetage.
Ce logiciel s’appelle « FramaWizard » et est situé dans `E:\dev\portables\Pack\DevToolsPortable\FramaWizard-2.0\`

Lancez-le : (`E:\dev\portables\Pack\DevToolsPortable\FramaWizard-2.0\FramaWizard.exe`)

Vous allez vous retrouver devant une interface graphique, vous demandant des informations.

Le plus simple est d’aller dans le menu « Fichier » et d’y choisir
« Nouvelle application », l’assistant vous demandera de rentrer le nom
de l’application portable en vous donnant un exemple.
Dans notre cas, nous allons donc entrer : `PNotesPortable`.

Astuce : *si vous rentrez juste PNotes, l’assistant complètera tout seul* ;)

Vous allez avoir alors différents champs se pré-remplir.

Complétez ensuite les différents champs. Si vous ne savez pas quoi mettre,
laissez le champs vide ou demandez sur le [forum de Framakey](https://framacolibri.org/c/qualite/framakey).

Vous aurez peut-être noté qu’un des boutons de droite s’est activé ?
Si le dossier indiqué dans le groupe « Code » vous convient, vous pouvez
cliquer sur le bouton « Transférer l’application ».
L’assistant vous demandera de lui indiquer le dossier où est
installé/décompressé l’application et se chargera de la copier au bon endroit.

À la fin de la copie, le bouton « Créer/Éditer le NSI » s’active et
l’assistant essaie de retrouver le nom de l’exécutable et sa version
(groupe « Code » ).

Si vous essayez de cliquer sur celui-ci vous recevrez un message
d’erreur (dans la barre de statut) et verrez une infobulle vous indiquant
les informations obligatoires manquantes.

Une fois les informations de l’onglet « Général » complétées, il
faudra s’occuper des images nécessaires à la réalisation de l’application portable.

## 2. Ajout des images

Dans l’onglet « Images », comme il s’agit d’une nouvelle application,
vous verrez deux grosses croix blanches sur fond rouge et de nombreuses
mentions « Absent ».
Il va falloir fournir deux fichiers à l’assistant : un pour les icônes
et logos et un autre pour les captures d’écrans.

L’idéal pour les icônes et logos est d’avoir un fichier au format PNG
d’une taille minimale de 256×256 pixels.
Plus petit ça passera aussi mais les grandes dimensions seront moins bien définies.

Vous pouvez essayer d’extraire une icône du programme à l’aide de
XNResourceEditor, situé dans le répertoire `E:\dev\portables\Pack\XNREPortable`.
Malheureusement ce programme s’accommode assez mal des icônes de type Vista en 256×256.
Vous pouvez également essayer avec [Anolis Resourcer](http://anolis.codeplex.com/).
Et il reste la possibilité la plus simple et qui fonctionne fréquemment :
trouver un fichier PNG dans la taille requise directement sur la Toile.

Pour la capture d’écran du logiciel c’est plus simple :
Il vous suffit de lancer le logiciel précédemment installé/décompressé
et d’en faire une capture d’écran. Une façon rapide est, par exemple,
avec la touche ImprEcran puis de copier le résultat dans Gimp, de rogner
et d’enregistrer au format PNG.
Il existe également d’autres logiciels (libres ou non) qui permettent
de capturer directement la fenêtre.

Une fois ces deux images disponibles, il suffit de cliquer sur chacun
des boutons ( … ) et d’indiquer à l’assistant l’image choisie.
Il se chargera de créer les miniatures et les icônes adéquates puis de
les placer au bon endroit.

## 3. Édition du fichier source du lanceur

Maintenant que vous avez renseigné toutes les informations nécessaires,
vous pouvez cliquez sur le bouton « Créer/Éditer le NSI ».
Ceci va provoquer le remplissage automatique d’un fichier source NSI
(et de fichiers auxiliaires) en fonction des données puis ouvrir
celui-ci dans Notepad++Portable.

Le logiciel étudié (PNotes) étant nativement portable et discret, il
vous suffit de vérifier que les données fournies ont bien été insérées.

Dans le cas d’un logiciel plus complexe, c’est dans ce fichier qu’il
faudra intervenir pour contourner le comportement habituel.
Exemple : votre logiciel sauvegarde sa configuration dans `C:\Documents and Settings\Pierre\Application Data\Nom_du_logiciel`.
Il « suffira » de décommenter la ligne `!define APPDATABACKUP` puis d’enregistrer.
Il s’agit d’un cas géré par le fichier source NSI proposé.
Les difficultés peuvent être bien plus grandes !

L’assistant se masque lorsque Notepad++Portable est lancé.
Il faut fermer ce dernier pour le faire réapparaître.

## 4. Génération du lanceur

Le bouton « Compiler le NSI » est maintenant activé.
Cliquez dessus pour lancer la génération du lanceur.
Le résultat de la compilation est affiché dans la barre de statut.
Vous noterez également que le nom du paquet et la taille se sont
automatiquement affichés.

L’application est en place, le lanceur est compilé, il ne reste donc
plus qu’à créer le paquet pour la distribution.
Ça tombe bien, le bouton « Paquet ZIP » est justement activé !

Cliquez dessus pour créer l’archive au format ZIP qui vous permettra
la distribution de votre application.
La barre de statut vous informera que votre paquet est disponible dans le dossier de FramaWizard.

Dans le dossier `E:\dev\portables\Pack\DevToolsPortable\FramaWizard-2.0\`,
vous trouverez le paquet mais aussi un dossier nommé `PNotesPortable`
qui contient votre première application portable.

Moment de vérité : lancez le fichier `PNotesPortable.exe` présent dans
`E:\dev\portables\Pack\DevToolsPortable\FramaWizard-2.0\PNotesPortable`.
Si le logiciel se lance normalement cela devrait marquer la fin de
la portabilisation.

À noter que si vous désirez gagner de la place et donc compresser
votre application, vous pouvez le faire à l’aide de FramaCompress.
Attention toutefois, il s’agit d’une opération qui peut rendre
inutilisable votre application portable !

*Astuce : réalisez une première version fonctionnelle, déplacez la du 
dossier de FramaWizard, relancez celui-ci mais choisissez « Mise à jour » dans le menu.
FramaWizard recopiera alors votre application dans son dossier.
Vous pourrez alors faire tous les tests voulus sans rien risquer* ;)

Prêts à soumettre votre travail pour publication ? [Allons-y…](portabiliser-tuto-6.html)  
[Retourner à la page précédente…](portabiliser-tuto-4.html)
