# Portabiliser un logiciel

Dans le but de pouvoir accélérer la mise en ligne des applications 
(et bien entendu d’augmenter le nombre d’applications disponibles), 
nous vous proposons de télécharger le Pack de portabilisation pour la Framakey.

Il s’agit d’un ensemble d’outils (libres et portables, évidemment !) 
qui facilitera la vie des ~~développeurs~~ bidouilleurs souhaitant 
portabiliser des applications.

Il se décompose comme suit.

Les outils indispensables :

 *  **RegshotPortable** : pour vérifier la discrétion d’une application comme les traces laissées dans la base de registre et le profil.
 *  **FramaWizahk**¹ : le successeur du FramaWizard du pack 9. C’est un assistant pour la création des paquets, il ne fait pas tout mais tente de simplifier énormément le travail.
 *  **Notepad++Portable**, pour l’édition des fichiers AutoHotkey, NSIS ou AutoIt et leur compilation (en liaison avec les autres utilitaires). Attention la version proposée dans le pack est plus complète que la version standard !
 *  **FKPortableCompiler**¹ : un compilateur autonome pour la création des lanceurs.
 *  **FramaCompressor**¹ : pour recompresser les exe, dll et archives jar. Il est utilisé par FramaWizahk mais est autonome. 

Les outils accessoires :

 *  **AutoHotkeyPortable** : un langage de script libre, utilisé dans Framafox et Framabird pour l’utilitaire de rebranding et dans les utilitaires de la FK 2.0.
 *  **SmartGUI**¹ : pour créer « facilement » des interfaces graphiques pour AutoHotkey.
 *  **NSISPortable** : pour compiler les « vieux » lanceurs , inclut de nombreux plugins.
 *  **XNREPortable** : éditeur de ressources (vous permettra par exemple d’extraire l’icône d’un logiciel). 

¹ Ces outils sont regroupés dans le dossier **DevToolsPortable**.

<p class="text-center">
  <a href="http://files.framakey.org/testing/main/tools/Pack_de_portabilisation_v10.0.0.8.7z" class="btn btn-default">
    <i class="fa fa-download fa-lg text-success"></i> Télécharger le pack (version 10.0.0.8 - 15.4 Mio)
  </a>
</p>

Vous trouverez une foule d’informations techniques dans le fichier `/DevToolsPortable/FramaWizahk/DefaultStructure/Other/Source/LogigrammeLanceur.odg`.
Et bien sûr, n’hésitez pas à poser vos questions sur [le forum](https://framacolibri.org/c/qualite/framakey).

En attendant la sortie d’un tutoriel qui tienne compte des changements apporté par ce pack,
vous pouvez vous référez à [celui de la version précédente](portabiliser-tuto-intro.html).

