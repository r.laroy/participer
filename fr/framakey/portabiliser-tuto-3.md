# Tester sa discrétion

## 1. Avant-propos

Les conditions de test idéales serait de n’avoir aucune application en 
fonctionnement à ce moment de la procédure (logiciel de courrier, 
logiciel de messagerie instantanée). 
Ce sont typiquement ces applications qui rendent plus difficile la 
lecture des résultats que nous allons obtenir.

## 2. Rentrons dans le vif du sujet
### 1. Cas d’un logiciel avec un installeur

Dans notre exemple, le fichier téléchargé n’est pas un installeur. 
Si vous récupérer un logiciel avec un installeur, une étape 
supplémentaire est nécessaire,
Avant d’exécuter le logiciel d’installation, lancez 
`E:\dev\portables\Pack\ResgshotPortable\ResgshotPortable.exe`

Cliquez sur « Premier passage » puis « Passage ». 
Patientez quelques secondes (il faut parfois jusqu’à une bonne minute 
pour scanner l’ensemble). Laissez le logiciel ouvert.

Maintenant, installez le logiciel que vous avez récupéré de façon 
classique (Suivant, suivant, enfin comme d’habitude, quoi).

Une fois installé, retournez dans RegshotPortable et cliquez sur 
« Deuxième passage » puis « Passage » (et patientez de nouveau).

Cliquez alors sur « Comparer » et visualisez ce beau fichier, il est 
automatiquement enregistré dans `E:\dev\portables\Pack\ResgshotPortable\Data\Shots`
(sous un nom certes légèrement ésotérique).

Regardons alors ce fichier : voir le paragraphe « Lecture du fichier Regshot »

Vous avez lu le paragraphe qui vous explique si l’étape est correcte ? 
Alors continuons.

Si à la lecture du paragraphe « Lecture du fichier Regshot » vous en 
concluez que l’installation s’est déroulé de manière discrète, alors 
il faut repérer le dossier d’installation du logiciel 
(la plupart du temps situé dans `C:\Program Files\Nom_du_logiciel`) 
pour l’indiquer à l’assistant plus tard.

Vous pouvez alors passer au paragraphe suivant « Cas d’un logiciel 
sans installeur » en sautant la première partie.

### 2. Cas d’un logiciel sans installeur
#### 1) Première partie

Vous avez récupéré PNotes_3_0_104.zip ? Dézippez le où bon vous semble, 
de préférence par trop éloigné de la racine d’un disque.

#### 2) Deuxième partie

Avant d’exécuter le programme, lancez `E:\dev\portables\Pack\ResgshotPortable\ResgshotPortable.exe`

Ajoutez au niveau du champ « Scan dir1[;dir2;…;dir nn] », en cliquant 
sur le bouton avec 3 points, le dossier `C:\Documents and Settings`

Cliquez ensuite sur « Premier passage » puis « Passage ». 
Patientez quelques secondes (il faut parfois jusqu’à une bonne minute 
pour scanner l’ensemble). Laissez le logiciel ouvert.

Exécutez alors le logiciel (c’est bon, vous vous rappelez où vous 
l’aviez décompressé ?)

Effectuez alors de nombreuses manipulations et surtout s’il y a des 
préférences ou des options, n’hésitez pas à les changer. 
En effet, ces données sont celles qui sont le plus susceptibles de 
venir perturber la discrétion d’un logiciel.

Une fois l’utilisation du logiciel terminé, fermez le, retournez dans 
RegshotPortable et cliquez sur « Deuxième passage » (et patientez de nouveau).

Cliquez alors sur « Comparer » et visualisez ce beau fichier 
(l’enregistrement est toujours automatique dans `E:\dev\portables\Pack\ResgshotPortable\Data\Shots`).

Regardons alors ce fichier : voir le paragraphe « Lecture du fichier Regshot »

#### 3) Lecture du fichier Regshot

Le fichier de résultat se présente sous la forme :

 *  entête : la version de Regshot, la date, l’utilisateur, …
 *  Les valeurs ajoutées dans la base de registres
 *  Les valeurs modifiées dans la base de registres
 *  Les fichiers ajoutés sur l’ordinateur (dans les dossiers indiqué au niveau du champs « Scan dir1[;dir2;…;dir nn] »)
 *  Les fichiers supprimés sur l’ordinateur (dans les dossiers indiqué au niveau du champs « Scan dir1[;dir2;…;dir nn] »)
 *  Les fichiers modifiés sur l’ordinateur (dans les dossiers indiqué au niveau du champs « Scan dir1[;dir2;…;dir nn] ») 

Concernant la base de registre, il va falloir identifier si les données 
ajoutées ou modifiées sont ou non sensibles. Chaque logiciel étant 
différent, il va falloir lire les modifications apportées.

Néanmoins, quelques cas se présentent régulièrement :

 *  Ajout ou modification de valeurs contenant `LastVisitedMRU` ou `OpenSaveMRU` dans le chemin.
    Ce sont des données non sensibles car il s’agit des dernières 
    applications ou fichiers lancés
 *  Ajout ou modification de valeurs contenant des adresses mémoire 
    (`0×00000001` par exemple) ou des données hexadécimales 
    (`F5 A0 F8 8E C0 ED 30` par exemple).
    Ce sont de nouveau des données non sensibles.
 *  Ajout ou modification de valeurs contenant le nom d’une option ou 
    d’un paramètre que vous avez modifié lors de l’utilisation du logiciel.
    Attention, ceci est un signe que le logiciel risque de ne pas être 
    portable nativement 

D’autres cas peuvent se présenter. En cas de doutes, n’hésitez pas à 
poser la question sur le [forum de Framakey](https://framacolibri.org/c/qualite/framakey).

Concernant les fichiers, là encore des cas généraux se présentent :

 *  Des fichiers ajoutés ou modifiés dans le répertoire 
    `C:\Documents and Settings\Nom_d_utilisateur\Recent\`.
    Ce ne sont pas des fichiers sensibles.
 *  Des fichiers ajoutés, suite à l’installation du logiciel dans le répertoire : 
    `C:\Documents and Settings\Nom_d_utilisateur\Bureau\` ou `C:\Documents and Settings\Nom_d_utilisateur\Menu Démarrer\`.
    Ce ne sont pas des fichiers sensibles, il s’agit simplement des raccourcis créés lors de l’installation
 *  Des fichiers modifiés portant comme nom : `software.LOG`, `Log.xml` 
    ou encore `NTUSER.DAT.LOG`. 
    Ce ne sont la plupart du temps pas des fichiers sensibles 

Encore une fois, des cas multiples et variés peuvent se présenter.
En cas de doute, direction le [forum de Framakey](https://framacolibri.org/c/qualite/framakey).
Si vous n’avez vu aucune données sensibles, vous pouvez alors considérer le logiciel comme discret.

Si vous étiez à l’étape « Cas d’un logiciel avec installeur », alors retournez-y. Sinon continuons notre petit parcours.

Prêts pour les tests de portabilité ? [Allons-y…](portabiliser-tuto-4.html)  
[Retourner à la page précédente…](portabiliser-tuto-2.html)
