# Proposer des articles pour le <b class="violet">Frama</b><b class="rouge">blog</b>

Il nous arrive de publier des articles rédigés par des personnes extérieures à l'association. La plupart du temps nous allons nous-même demander la (re-)publication de tel ou tel article à leur auteur·trice.

Néanmoins, vous pouvez [nous contacter](https://contact.framasoft.org/#framablog) afin de nous proposer votre ou vos textes.

## Avant de nos proposer un texte&hellip;

Voici une petite <i lang="en">check-list</i> à vérifier avant même de nous contacter pour nous proposer votre (ou vos) article(s).

* J'ai bien lu (et ma prose se conforme à) [la ligne éditoriale du blog](index.html#ligne-éditoriale-du-framablog)&nbsp;;
* Mon texte est proposé sous une licence libre&nbsp;;
* Mon texte traite du Libre, de sa culture, de nos libertés numériques, etc.&nbsp;;
* Mon texte est disponible sous un format facile à reproduire/mettre en page (.txt, .md ou .html de préférence)&nbsp;;
* Je suis prêt à travailler en collaboration avec des membres de <b class="violet">Frama</b><b class="orange">soft</b> sur ce texte et suis donc ouvert·e aux modifications et éventuelles critiques constructives&nbsp;;
* J'ai conscience que le <b class="violet">Frama</b><b class="rouge">blog</b> dépend principalement d'énergies bénévoles, et donc que personne n'est à mon service (tout comme je ne suis au service de personne)&nbsp;;
* Je connais la règle du [Yakafokon](http://yakafokon.detected.fr) en rigueur chez <b class="violet">Frama</b><b class="orange">soft</b> (mais j'ai quand même cliqué sur le lien pour en être bien sûr·e)&nbsp;;
* Je suis dans une intention de partage, de vulgarisation (au sens noble) et/ou d'éducation populaire aux idées exposées dans mon texte&nbsp;;
* Je ne suis pas dans une démarche publicitaire, de «&nbsp;partenariat promotionnel&nbsp;», de marketing SEO, de liens croisés, d'auto-promotion éhontée, etc. (systématiquement refusé&hellip; Ne perdez donc pas votre temps&nbsp;: il est précieux).

## Articles exceptionnels

Vous désirez nous proposer un article, de manière exceptionnelle&nbsp;?
La <i lang="en">check-list</i> ci dessus ne vous a même pas fait peur&nbsp;?

Parfait&nbsp;! voici comment faire, selon les cas de figure&nbsp;:

* **Vous êtes l'auteur·trice de l'article&nbsp;?** [Contactez-nous](https://contact.framasoft.org/#framablog) en mentionnant&nbsp;:
  * Votre intention, pourquoi vous nous le proposez&nbsp;;
  * L'article en lien/pièce jointe&nbsp;;
  * La licence libre de votre choix (si autre que CC-BY-SA)&nbsp;;
  * L'adresse email que nous pourrons utiliser pour collaborer ensemble.
* **Vous avez repéré un article d'un·e autre ateur·trice&nbsp;?**
  * Si vous désirez que <b class="violet">Frama</b><b class="rouge">lang</b> le traduise, vous êtes au mauvais endroit, il vous faut [aller voir ici](/framalang/index.html).
  * S'il n'est pas libre ou que vous n'avez pas l'autorisation écrite, ce n'est pas légal, nous ne le publierons donc pas -_-&nbsp;;
  * Si vous n'êtes pas prêt·e à mouiller la chemise pour le mettre en page sur le blog (règle du [Yakafokon](http://yakafokon.detected.fr)), sachez que nous n'avons même pas le temps de (re-)publier tous les articles que nous voudrions&hellip; Bref ça ne servira à rien, désolé. Vous pouvez par contre nous le partager sur les réseaux sociaux&nbsp;;)&nbsp;;
  * Sinon&nbsp;: impec, il ne vous reste plus qu'à [nous contacter](https://contact.framasoft.org/#framablog)&nbsp;!


## Les claviers invités

Nous venons tout juste d'ouvrir une section [Claviers Invités](https://framablog.org/category/libren-vrac/claviers-invites/) dans le <b class="violet">Frama</b><b class="rouge">blog</b>.

<div class="clearfix">
<div class="alert alert-info">
	<strong>ATTENTION&nbsp;:</strong>
	<p>Cette expérimentation (et la démarche qui la sous-tend) sont toutes récentes&nbsp;: nous essuyons les plâtres&hellip; Nous déterminons donc les usages au fur et à mesure que nous avançons.</p>
	<p>Il ne faut donc pas craindre de découvrir cela ensemble si vous désirez vous joindre à nous&nbsp;!</p>
</div>
</div>

Le but est de s'ouvrir à une diversité de voix et de sujets qui pourraient intéresser le lectorat de ce blog, sans forcément représenter les vues/la pensée/etc. de <b class="violet">Frama</b><b class="orange">soft</b>.

Si l'aventure vous tente, vous êtes les bienvenu·e·s. Nous recommandons cependant&nbsp;:
* D'avoir une connaissance du monde libriste, des enjeux du numérique, etc.&nbsp;;
* D'avoir envie de vous adresser à un public béotien tout en étant lu·e par des personnes spécialisées dans ces domaines&nbsp;;
* De ne pas avoir peur des (d'apprendre les) méandres de Wordpress ^^&nbsp;!

Là aussi, pour proposer vos mots, [contactez-nous](https://contact.framasoft.org/#framablog).
