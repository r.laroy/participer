# Premiers pas sur Framalibre

## Plusieurs façons de naviguer dans l'annuaire

Suivant l'information que l'on recherche, plusieurs options s'offrent à nous dès la page d'accueil

![capture page d'accueil](images/framalibre01.png "Page d'accueil de Framalibre")

### Rechercher une ressource

La barre de recherche est là si vous voulez rechercher directement une ressource (un logiciel, un livre, une asso, etc.) par son nom ou par son *tag* (les étiquettes, ou mots-clés, que l'on met sur les notices).

### Naviguer dans les catégories

Le menu du haut permet d'aller chercher la ressource désirée selon la catégorie où elle se trouve. On distingue 4 catégories principales :
  1. **S'informer** : On y trouvera des liens vers des sites pour protéger sa vie privée ou d'autres campagnes d'informations, mais aussi un glossaire, ou des notices sur les différentes licences libres existantes.
  2. **S'équiper** : Ici, c'est le lieu pour découvrir des objets libres, de nombreux logiciels, mais aussi des jeux de données sous licence libre.
  3. **Se cultiver** : Livres, Images, Musique, Films... Toute œuvre culturelle placée sous licence libre par son, sa ou ses auteur·ice·s peut s'inclure dans cette catégorie.
  4. **S'entourer** : Le libre, ce sont avant tout des personnes, qui souvent se regroupent auprès d'un projet. Que ce soit en collectif, association, entreprise, etc. ces initiatives ont leur place dans cette catégorie.


### Se laisser surprendre

Dès la page d'accueil, des recommandations vous permettent de découvrir des ressources libres au hasard de votre venue. Que ce soient les dernières notices entrées dans l'annuaire ou nos références, ou les suggestions au pied des notices que vous visiterez, c'est un premier appel à la navigation dans l'annuaire.

Notez que chacun de ces carrés vous permet d'aller lire directement la notice, de conserver la notice dans votre registre, ou même d'aller directement sur le site officiel de la ressource.

### Les liens pratiques sont à droite !

La colonne de droite ne s'affiche que sur la page d'accueil, et sert à accéder rapidement à des liens pratiques. Au delà de ses informations de compte (voir plus bas), on y trouve des boutons pour :

  * trouver des [alternatives libres](https://framalibre.org/alternative-free-softwares) aux logiciels privateurs connus
  * [ajouter une notice](creer-modifier-une-notice.html) à l'annuaire
  * [signaler un contenu](https://framalibre.org/node/308) (parce qu'il est incorrect, inadapté...)
  * lire les [actualités du Libre](https://framalibre.org/content/les-actualit%C3%A9s-du-libre)
  * consulter [l'ensemble des notices](https://framalibre.org/touteslesnotices) du site
  * accéder aux [flux RSS](https://framalibre.org/?q=rss.xml) du site

## Découvrons une notice

![capture Firefox, sans compte connecté](images/framalibre07.png "La notice consacrée à Firefox")

Lorsqu'on clique sur la notice d'une ressource, plusieurs informations pratiques sont à notre disposition dans la colonne principale...

1. En haut, le chemin de la catégorie (menu, sous menus) où l'on peut retrouver la notice
2. les onglets "Voir", "Modifier" et "Révisions" pour les diverses évolutions de la notice dans le temps
3. Une illustration au dessus des informations pratiques
4. le texte de présentation de la notice et les tags (mots-clés) qui l'accompagnent
5. les liens pratique (a minima le site officiel de la ressource, éventuellement la page Wikipédia, etc.)

Et autour, on peut retrouver :

  * Des informations liées à la catégorie (colonne de droite
  * Des suggestions de notices similaires (sous les infos principales)

> NOTE : en cliquant sur l'utilisateur qui a créé la notice, vous vous rendez sur la page de son profil public. Vous pouvez ainsi le contacter via un formulaire si vous souhaite lui proposer des modifications que vous souhaitez pas effectuer vous-même.
