# Documentation

## Utiliser Framalibre

* [Premiers pas sur Framalibre](premiers-pas.html)
* [De bonnes raisons pour se créer un compte](se-creer-un-compte.html)
* [Comment créer ou modifier une notice ?](creer-modifier-une-notice.html)


## Liens externes

 * [L'annuaire Framalibre](https://framalibre.org)
 * [La foire aux questions Framalibre](https://framalibre.org/faq-page)
 * [Signaler un bug / proposer une amélioration technique](https://framagit.org/framasoft/framalibre/issues)